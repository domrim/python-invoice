import datetime

import invoice

customer = invoice.Customer(
    first_name="Britzel",
    last_name="Bär",
    street="Musterstraße",
    house_no="42",
    postcode="12345",
    city="Hinter den Bergen",
)

treasurer = "Dagobert Duck"
invoice_no = "202010-001"

config = invoice.InvoiceConfig(lang="de_DE", tax_rates=invoice.TaxRates(16, 5))

item1 = invoice.InvoiceItem(quantity="10 Seiten", description="Protokolle", tax="reduced", gross=10, config=config)
item2 = invoice.InvoiceItem(quantity="1", description="Porto", tax="none", net=1.55, config=config)
item3 = invoice.InvoiceItem(quantity="1", description="Stadtmobilnutzung KA-ST XXXX",
                            service_date_start=datetime.date.today(), tax="full", net=50.00, config=config)
item4 = invoice.InvoiceItem(quantity="1", description="Stadtmobilnutzung KA-ST XXXX",
                            service_date_start=datetime.date.today(), service_date_end=datetime.date.today(),
                            tax="full", gross=100, config=config)

content = invoice.Invoice(
    config=config,
    customer=customer,
    no=invoice_no
)

content.add_items([item1, item2, item3, item4])
text = invoice.render_invoice(content, treasurer, config)
invoice.compile_invoice(text, "test.pdf")
