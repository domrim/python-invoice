from dataclasses import dataclass, field, InitVar
from typing import List
from invoice.exceptions import MissingData, InvoiceException
from invoice.invoice_config import InvoiceConfig
import datetime


@dataclass
class Customer:
    first_name: str
    last_name: str
    street: str
    house_no: str
    postcode: str
    city: str
    address_supplement: str = None


@dataclass
class InvoiceItem:
    quantity: str
    description: str
    tax: str
    net: float = None
    gross: float = None
    config: InvoiceConfig = InvoiceConfig()
    service_date_start: datetime.date = None
    service_date_end: datetime.date = None

    def __post_init__(self):
        """Validate Data and calculate missing data"""

        if self.net is None and self.gross is None:
            raise MissingData("At least one of gross and net have to be defined.")

        # Calculate net from gross (tax_rates are given in percent)
        if self.net is None:
            self.net = float(self.gross / (100 + getattr(self.config.tax_rates, self.tax)) * 100)

        # Calculate gross from net (tax_rates are given in percent)
        if self.gross is None:
            self.gross = float(self.net * (100 + getattr(self.config.tax_rates, self.tax)) / 100)

        # Round currency values
        self.gross = round(self.gross, 2)
        self.net = round(self.net, 2)

        # Check if gross and net are matching
        if self.net != round((self.gross / (100 + getattr(self.config.tax_rates, self.tax)) * 100), 2):
            msg = (
                f"Net and gross do not match for given VAT.\n"
                f"vat: {self.tax} / {getattr(self.config.tax_rates, self.tax)}\n"
                f"net: {self.net}\n"
                f"gross: {self.gross}"
            )
            raise InvoiceException(msg)


@dataclass
class InvoiceData:
    no: str
    customer: Customer
    config: InvoiceConfig = InvoiceConfig()
    items: List[InvoiceItem] = field(default_factory=list)
    date: datetime.date = datetime.date.today()
    payment_term: InitVar[int] = 14

    def __post_init__(self, payment_term):
        """Calculate Duedate"""
        self.duedate = self.date + datetime.timedelta(days=payment_term)

    def total(self) -> float:
        total = sum([item.gross for item in self.items])
        return total

    def sum(self, tax: int, val: str) -> float:
        if val in ["gross", "net"]:
            calc = sum([getattr(item, val) for item in self.items if item.tax == tax])
        elif val == "tax":
            gross = sum([item.gross for item in self.items if item.tax == tax])
            net = sum([item.net for item in self.items if item.tax == tax])
            calc = gross - net
        else:
            raise InvoiceException(f"Invoice Items have no value '{val}'")
        result = round(calc, 2)
        return result

    def add_item(self, item: InvoiceItem):
        self.items.append(item)

    def add_items(self, items: List[InvoiceItem]):
        for item in items:
            self.add_item(item)

    def list_items(self):
        return [item for item in self.items]

    def remove_item(self, index):
        self.items.pop(index)

