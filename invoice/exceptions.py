
class InvoiceException(Exception):
    pass


class MissingData(InvoiceException):
    pass
