from latex import build_pdf, LatexBuildError
import os

static_dir = os.path.join(os.path.dirname(__file__), "static")


def compile_invoice(template: str, output: str):
    """Takes rendered template and compiles it with all files in static dir"""
    if not output.endswith(".pdf"):
        output += ".pdf"
    try:
        pdf = build_pdf(template, texinputs=[static_dir, ''], builder='pdflatex')
        pdf.save_to(output)
    except LatexBuildError as e:
        for err in e.get_errors():
            print('Error in {0[filename]}, line {0[line]}: {0[error]}'.format(err))
            # also print one line of context
            print('    {}'.format(err['context'][1]))
            print("")
