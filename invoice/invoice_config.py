from dataclasses import dataclass


@dataclass
class TaxRates:
    full: int = 19
    reduced: int = 7
    none: int = 0


@dataclass
class InvoiceConfig:
    lang: str = "de_DE"
    formal: bool = False
    tax_rates: TaxRates = TaxRates()

    def get_latex_lang(self):
        mapping = {
            "de_DE": "ngerman",
            "en_US": "english"
        }
        return mapping[self.lang]
