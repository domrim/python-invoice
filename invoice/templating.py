import os
from jinja2.loaders import FileSystemLoader
from latex.jinja2 import make_env
from babel.numbers import format_decimal
from babel.support import Translations
from babel.dates import format_date
from . import Invoice
from . import InvoiceConfig


template_dir = os.path.join(os.path.dirname(__file__), "templates")
translations_dir = os.path.join(os.path.dirname(__file__), "translations")


def _tex_escape(s):
    """Escapes common problematic characters in TeX code like
    _, &."""
    tex_replacements = [
        ('{', '\\{'),
        ('}', '\\}'),
        ('[', '{[}'),
        (']', '{]}'),
        ('\\', '\\textbackslash{}'),
        ('$', '\\$'),
        ('%', '\\%'),
        ('&', '\\&'),
        ('#', '\\#'),
        ('_', '\\_'),
        ('~', '\\textasciitilde{}'),
        ('^', '\\textasciicircum{}'),
        ('<', '\\textless{}'),
        ('>', '\\textgreater{}'),
    ]
    mapping = dict((ord(char), rep) for char, rep in tex_replacements)
    s = str(s).translate(mapping)
    return s


def _eur_format(i, config: InvoiceConfig):
    return f"\\EUR{{{format_decimal(i, format='#,##0.00', locale=config.lang)}}}"


def _format_date(date, config: InvoiceConfig):
    return format_date(date, locale=config.lang)


def _getattr(obj, attr):
    return getattr(obj, attr)


def render_invoice(invoice: Invoice, treasurer: str, config: InvoiceConfig) -> str:
    env = make_env(loader=FileSystemLoader(os.path.join(template_dir)), extensions=['jinja2.ext.i18n'])
    env.filters["tex"] = _tex_escape
    env.filters["eur"] = _eur_format
    env.filters["format_date"] = _format_date
    env.filters["getattr"] = _getattr

    env.newstyle_gettext = True
    translations = Translations.load(translations_dir, config.lang)
    env.install_gettext_translations(translations)

    tpl = env.get_template("invoice.tex")

    rendered = tpl.render(
        invoice=invoice,
        treasurer=treasurer,
        config=config,
    )
    return rendered
