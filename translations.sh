#!/usr/bin/env bash
# Colors
BLUE='\033[34m'
WHITE='\033[37m'
NC='\033[0m'
NC='\033[0m'

function usage() {
  cat << EOF
Usage: $0 [command]
available commands:
  - extract: extracts msgids from template file
  - update: runs update for translations
  - compile: compiles translations
  - all: run extract, update and compile
EOF
}

if [ "$#" -ne 1 ]; then
  echo "Invalid number of parameters."
  usage
  exit
fi

SCRIPTPATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 || return ; pwd -P )"
DOMAIN="invoice/translations"

if [ "$1" == "extract" ] || [ "$1" == "all" ]; then
  printf "${WHITE}pybabel extract -F %s/babel.cfg -o %s/%s/messages.pot --omit-header %s\n${BLUE}" "$SCRIPTPATH" "$SCRIPTPATH" "$SCRIPTPATH" "$SCRIPTPATH"
  pybabel extract -F "$SCRIPTPATH/babel.cfg" -o "$SCRIPTPATH/$DOMAIN/messages.pot" --omit-header "$SCRIPTPATH"
fi
if [ "$1" == "update" ] || [ "$1" == "all" ]; then
  printf "${WHITE}pybabel update -i %s/%s/messages.pot  -d %s/%s --omit-header\n${BLUE}" "$SCRIPTPATH" "$DOMAIN" "$SCRIPTPATH" "$DOMAIN"
  pybabel update -i "$SCRIPTPATH/$DOMAIN/messages.pot"  -d "$SCRIPTPATH/$DOMAIN" --omit-header
fi
if [ "$1" == "compile" ] || [ "$1" == "all" ]; then
  printf "${WHITE}pybabel compile -d %s/%s -f\n${BLUE}" "$SCRIPTPATH" "$DOMAIN"
  pybabel compile -f -d "$SCRIPTPATH/$DOMAIN"
fi
# shellcheck disable=SC2059
printf "${NC}"